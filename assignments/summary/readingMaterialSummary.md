# Industrial Revolution Flowgraph:  
> Industrial revolution is defined as the changes in manufacturing and transportation that began with fewer things being made by hand but instead made using machines in larger-scale factories.  



![](PhotosinUse/a4f4afcdd410028a61365bfccceccac9.jpg)  
> A **PLC** is an example of _real-time system_ ,since the output of the system controlled by the plc depends on the input condition.  


# Understanding Industrial 3.0:  
* The main highlight was **PLC**(Programmable logic controller) which can be programmed using ladder diagram and data can be mainly stored in excels or CSV's in PC's which can be connected to plc using serial port.

### Architecture:  

1. **Input Devices**: These are useful in converting any mechanical work into electrical signal which can be either digital or analog depeneding upon the sensor type.  

    * Sensors /Actuators  
        * Field transmitters for pressure,temperature ,flow,level,etc.  
        * Photoelectric Sensors  
        * Pressure switches,Push buttons,Vaccum switches,etc  
        * Encoders  
    ![](PhotosinUse/industrial-digital-sensors.png)  

> After Converting into electrical signal ,now its time to analyize it ,or study the signal ,for this first it need to be transfer to System which we installed in Industry 3.0 i.e PLC,CNC,DCS,etc.  

2. **Data Transfer/Fieldbus**:We take output of sensors/actuators and transfer to Our system using any one of this protocol:  
    * Communication Protocol  
        * ModBus  
        * ProfiBus/Industrial Ethernet  
        * Ethercat  

> Now after getting response onto our system ,its time to operate our equipments.  

3. **Output Modules**:
    * Solenoid Valves
    * Motor shalves
    * Motor Drives/Motor starters

> Simulataneously we can plot this data in graph or store in excels also,using SCada,Hmi ERP,etc.  

# Industrial Revolution 4.0  
when Industry 3.0 system installed is connected  to internet is known as Industry 4.0. It focuses mainly on interconnectivity,machine learning,automation and real time data.  
> It is refers to as **IIot** (Industrial internet of Things) or smart manufacturing.

# Industrial Lookout of 4.0   

![](PhotosinUse/isometric-scheme-en.png)  

# How can we convert industry 3.0 to 4.0??  
By Converting industrial 3.0 protocol into industrial 4.0 protocol ,we can able to store our data in cloud,which can be accessible at any point we want.Some of this industrial 4.0 protocols are:  
* Message Queuing Telemetry Transport (MQTT)  
* Advanced Message Queuing Protocol (AMQP)
* WebSocketts
* Restfull Api  

# Architecture of 4.0:  
![](PhotosinUse/industrial4.0.png )  
> Edge takes data from the controllers and convert it to the protocols that the cloud understands and sends the data from controller to cloud so that consumer as well as owner have same copy of data anytime they want.  

# Problems with Industry 4.0 upgradation:  
1. **`Cost`**: Industry 4.0 devices are costly.  
2. **`Downtime`**:While upgradtion factory may need to closed,which would affect the revenue.  
3. **`Reliablity`**:Investing in a devices which are yet to be completely tested in market as secure device to be installed.  

#### Solution to above problems:  
* Get data from Industry 3.0 devices/meters/sensors without changes to the original device.    
* And then send the data to the Cloud using Industry 4.0 devices.  

# How to make your own Industrial IOt Product:  
1. Identify most popular Industry 3.0 devices.
2. Study the protocols that these devices communicate.  
3. Get data from these devices.  
4. Send the data to cloud for industry 4.0  

# Cloud Data Storing Platforms:  
> After the data is send to internet,it can be analyize with online platforms available.

* **`TSDB Tools`**:Store Your Data in time series databases.Iot TSDB tools are:
    * Promotheus
    * InfluxDB  


* **`IOT Dashboards`**:View all your datas into beautiful dashboards.Iot dashboards available are:  
    * Grafana
    * ThingsBoard  


* **`IOT Platforms`**:An IoT platform enables IoT device and endpoint management, connectivity and network management, data management, processing and analysis, application development, security, access control, monitoring, event processing and interfacing/integration.Analyse Your data in these platforms:  
    * AWS IOT
    * Google IOT
    * Azure IOT
    * ThingsBoard  

* **`Get Alerts/Notifications`**:Gets alerts/SMS based on your data using these Platforms:  
    * Zaiper 
    * Twilio      















        
          


